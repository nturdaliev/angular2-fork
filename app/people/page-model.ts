/**
 * Created by nursultan on 3/11/16.
 */
export interface PageModel {
    icon:string;
    title:string;
    RouterLink:string[]
}