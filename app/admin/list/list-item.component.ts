import {Component} from "angular2/core";
import {ListModel} from "./ListModel";
import {ListService} from "./list.service";
import {HTTP_PROVIDERS} from "angular2/http";
import {Container} from "../../Container";
import {OnInit} from "angular2/core";
import {RouteParams} from "angular2/router";
import {ListItem} from "./ListItem";
import {Router} from "angular2/router";
import {AfterContentInit} from "angular2/core";
import {RouterLink} from "angular2/router";
import {BreadcrumbService} from "../../services/breadcrumb.service";
import {BreadcrumbComponent} from "../../breadcrumb/breadcrumb.component";
import {DropDownButton} from "../../directives/dropdown-button.directive";
/**
 * Created by nursultan on 3/2/16.
 */
@Component({
    selector: 'list-item-component',
    templateUrl: 'app/admin/list/list-item.html',
    styleUrls: ['app/admin/list/list.css'],
    directives: [RouterLink, BreadcrumbComponent, DropDownButton],
    providers: [ListService, HTTP_PROVIDERS, Container]
})
export class ListItemComponent implements OnInit, AfterContentInit {

    private list:ListModel;
    public saving;
    public sortingInitialized:boolean;

    constructor(private listService:ListService,
                private routeParams:RouteParams,
                private router:Router,
                private _breadcrumbService:BreadcrumbService) {
    }

    ngAfterContentInit():any {

        if (this.sortingInitialized) {
            return;
        }
        this.makeSortable();
    }

    makeSortable() {
        //console.log('Making sortable');
        setTimeout(()=> {
            var el = document.getElementById('item-container');
            if (!el) {
                return;
            }
            Sortable.create(el, {
                animation: 150,
                handle: '.drag-handle',
                draggable: '.item-row',
                onEnd: (event)=> {
                    this.reorder(event.oldIndex, event.newIndex);
                }
            });
        }, 1000);
        this.sortingInitialized = true;
    }

    reorder(oldIndex:number, newIndex:number) {
        let temp = jQuery.extend({}, this.list.Items[oldIndex]);
        if (oldIndex < newIndex) {
            for (let i = oldIndex; i < newIndex; i++) {
                this.list.Items[i] = this.list.Items[i + 1];
            }
            this.list.Items[newIndex] = temp;
        } else {
            for (let i = oldIndex; i > newIndex; i--) {
                this.list.Items[i] = this.list.Items[i - 1];
            }
            this.list.Items[newIndex] = temp;
        }
    }

    ngOnInit():any {
        let id = this.routeParams.get('id');

        this.sortingInitialized = false;
        if (id === 'new') {
            this.list = {
                ListName: '',
                BuiltIn: false,
                Items: [],

            }
        } else {
            this.listService.getListItems(id)
                .subscribe(list => {
                    this.list = list;
                    var newValue = {key: 'ListItem', last: list.ListName};
                    this._breadcrumbService.broadcastComponentChange(newValue);
                });
        }
        return true;
    }

    addListItem() {
        this.list.Items.push(<ListItem>{Code: '', Title: ''})
    }

    onCancel() {
        this.router.navigate(['Lists']);
    }

    removeItem(index) {
        this.list.Items.splice(index, 1);
    }

    onSubmit() {
        this.saving = true;
        if (this.list.ID === undefined) {
            this.createList();
        } else {
            this.updateList();
        }
    }

    createList() {
        this.listService.post(this.list)
            .subscribe(newList => {
                Materialize.toast("New list has been successfully created", 4000);
                this.router.navigate(['Lists']);
            }, error => {
                Materialize.toast("An error occurred, please try again", 4000);
            });
    }

    updateList() {
        this.listService.putList(this.list).subscribe(list => {
                this.list = list;
                this.saving = false;
                Materialize.toast('Successfully saved!', 2000);
                this.router.navigate(['Lists']);
            },
            error => {
                this.saving = false;
                Materialize.toast('Invalid data', 2000);
            });
    }

    removeList() {
        jQuery('#modal-delete').openModal();
    }

    onDeleteYes() {
        jQuery('#modal-delete').closeModal();
        this.listService.delete(this.list)
            .subscribe(res => {
                this.router.navigate(['Lists']);
                Materialize.toast('List has been successfully successfully deleted', 4000);
            }, error => {
                Materialize.toast('An error occurred', 4000);
            });
    }

    onBlur(el, item:ListItem, property) {
        if (item[property] == el.innerText) {
            return;
        }
        item[property] = el.innerText;
    }
}