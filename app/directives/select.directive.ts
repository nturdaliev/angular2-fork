import {Directive, ElementRef, Input} from 'angular2/core';
@Directive({
    selector: '[materialize-select]',
})
export class SelectDirective {
    @Input('materialize-select') element:string;

    constructor(private el:ElementRef) {
    }

    ngAfterViewInit():any {
        jQuery(this.el.nativeElement).material_select();
    }
}