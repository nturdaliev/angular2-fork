import {Directive, ElementRef, Input} from 'angular2/core';
import {StringService} from "../services/string.service";
@Directive({
    selector: '[normalized-id]',
})
export class NormalizedIdDirective {
    @Input('normalized-id') element:string;

    constructor(private el:ElementRef, private stringService:StringService) {
    }

    ngAfterViewInit():any {
        jQuery(this.el.nativeElement).attr('id', this.stringService.normalize(this.element));
    }
}