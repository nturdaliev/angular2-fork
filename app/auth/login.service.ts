/**
 * Created by nursultan on 3/1/16.
 */
import {Component} from "angular2/core";
import {Injectable} from "angular2/core";
import {Http} from "angular2/http";
import {LoginModel} from "./LoginModel";
import {Container} from "../Container";
import {Headers} from "angular2/http";
import {RequestOptions} from "angular2/http";
import {Router} from "angular2/router";
@Injectable()
export class LoginService {

    constructor(private http:Http, private container:Container) {
    }

    authenticate(loginModel:LoginModel):any {

        let body = JSON.stringify(loginModel);
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});

        return this.http.post(this.container.baseUrl + '/login', body, options);
    }
}