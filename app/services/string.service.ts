import {Injectable} from "angular2/core";
@Injectable()
export class StringService {
    normalize(str) {
        if (!str) {
            return '';
        }
        return str.replace(' ', '_').toLocaleLowerCase();
    }
}