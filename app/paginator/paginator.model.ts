export interface  PaginatorModel {
    count:number;
    pageCount:number;
    currentIndex:number;
    pageSize:number;
    lastIndex:number;
    indexList:number[];
}