import {Component} from "angular2/core";
import {OnInit} from "angular2/core";
import {PropertyControlService} from "./property-control.service";
import {ControlGroup} from "angular2/common";
import {Input} from "angular2/core";
import {TabModel} from "./sub-components/tab.model";
import {FormInputComponent} from "./form-input.component";
import {TabDirective} from "../directives/tab.directive";
import {PersonService} from "../person/person.service";
import {RouteParams} from "angular2/router";
import {ProfileModel} from "./sub-components/profile-model";
@Component({
    selector: 'new-person-form',
    templateUrl: 'app/people/new-person-form.component.html',
    directives: [FormInputComponent, TabDirective],
    providers: [PropertyControlService]
})
export class NewPersonFormComponent implements OnInit {
    @Input('profileModel') profileModel:ProfileModel;
    @Input('tabNames') tabNames:TabModel[] = [];

    public form:ControlGroup;
    public id:string;
    public personType:string;
    public submitting:boolean;

    constructor(public propertyControlService:PropertyControlService,
                private personService:PersonService,
                private routeParams:RouteParams) {

    }

    ngOnInit() {
        this.id = this.routeParams.get('id');
        this.personType = this.routeParams.get('personType');
        this.submitting = false;
        this.form = this.propertyControlService.toControlGroup(this.profileModel.Properties);
    }

    public onSubmit() {
        this.submitting = true;
        this.personService.create(this.personType, this.profileModel)
            .subscribe(response => {
                jQuery('#modal-new-person').closeModal();
                window.location.reload();
            }, error=> {
                Materialize.toast("An error occurred, please try again", 4000);
            });
    }
}