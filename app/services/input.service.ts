import {Injectable} from "angular2/core";
@Injectable()
export class InputService {

    getInputs() {
        return ['date', 'text', 'number', 'email', 'datetime']
    }

    insideInputs(input):boolean {
        return !!(this.getInputs().indexOf(input) + 1);
    }
}