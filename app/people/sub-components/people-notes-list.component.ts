import {Component} from "angular2/core";
import {NoteService} from "./services/note.service";
import {NoteModel} from "./model/note.model";
import {OnInit} from "angular2/core";
import {RouteParams} from "angular2/router";
import {RouterLink} from "angular2/router";
import {Injector} from "angular2/core";
import {PeopleDetailParentComponent} from "../peope-detail-parent.component";
import {MaterializeTextarea} from "../../directives/textarea.directive";
@Component({
    selector: 'people-notes-list',
    templateUrl: 'app/people/sub-components/people-notes-list.component.html',
    directives: [RouterLink, MaterializeTextarea]
})
export class PeopleNotesListComponent implements OnInit {
    public notes:NoteModel[];
    public personID:string;
    public personType:string;
    public newNote:NoteModel;
    private editing:boolean;

    constructor(public noteService:NoteService, public injector:Injector) {
    }

    ngOnInit() {
        let parentComponent = this.injector.get(PeopleDetailParentComponent);
        this.personType = parentComponent.personType;
        this.personID = parentComponent.personID;

        this.getNotes();
    }

    getNotes() {
        this.noteService.get(this.personType, this.personID)
            .subscribe(response => this.notes = response);
    }

    onDelete(note:NoteModel) {
        this.noteService.delete(this.personType, this.personID, note.ID)
            .subscribe(response => {
                    this.getNotes();
                    Materialize.toast('Successfully deleted', 2000);
                },
                error=>error);
    }

    initNote(note?:NoteModel) {
        this.newNote = note ? Object.assign({}, note) : <NoteModel>{Note: ''};
    }

    onNewNoteClick(note:NoteModel) {
        this.editing = !!note;
        this.initNote(note);
        jQuery('#modal-new-note').openModal();
        this.autoResize();
    }

    onModalCancel() {
        this.newNote = null;
        jQuery('#modal-new-note').closeModal();
    }

    autoResize() {
        setTimeout(()=> {
            $('textarea').trigger('focus');
        }, 200);
    }

    onSubmit() {
        if (this.editing) {
            this.noteService.put(this.personType, this.personID, this.newNote)
                .subscribe(response => {
                    this.getNotes();
                    Materialize.toast('Successfully updated', 2000);
                    this.onModalCancel();
                }, error => error);
        } else {
            this.noteService.post(this.personType, this.personID, this.newNote)
                .subscribe(response => {
                    this.getNotes();
                    Materialize.toast('Successfully created', 2000);
                    this.onModalCancel();
                }, error => error);
        }
    }

}