import {Injectable} from "angular2/core";
import {Http} from "angular2/http";
import {Container} from "../../Container";
import {ListModel} from "./ListModel";
import {Observable} from "rxjs/Observable";
import {Response} from "angular2/http";
import {RequestOptions} from "angular2/http";
import {Headers} from "angular2/http";
/**
 * Created by nursultan on 3/2/16.
 */
@Injectable()
export class ListService {
    constructor(private http:Http, private container:Container) {
    }

    getLists() {
        let headers = new Headers({'Authorization': localStorage.getItem('token')});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.container.baseUrl + '/lists', options)
            .map(res => <ListModel[]> res.json())
            .catch(this.handleError);
    }

    private handleError(error:Response) {
        // in a real world app, we may send the error to some remote logging infrastructure
        // instead of just logging it to the console
        //console.log(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    getListItems(id) {

        let headers = new Headers({'Authorization': localStorage.getItem('token')});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.container.baseUrl + "/lists/" + id, options)
            .map(res => <ListModel> res.json())
            .catch(this.handleError);
    }

    post(list:ListModel):any {
        let body = JSON.stringify(list);
        let headers = new Headers({'Authorization': localStorage.getItem('token'), 'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.post(this.container.baseUrl + '/lists', body, options)
            .map(res => <ListModel> res.json())
            .catch(this.handleError);
    }

    putList(list:ListModel):any {
        let body = JSON.stringify(list);
        let headers = new Headers({'Authorization': localStorage.getItem('token'), 'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.put(this.container.baseUrl + '/lists/' + list.ID, body, options)
            .map(res => <ListModel> res.json())
            .catch(this.handleError);
    }

    delete(list:ListModel):any {
        let headers = new Headers({'Authorization': localStorage.getItem('token'), 'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.delete(this.container.baseUrl + '/lists/' + list.ID, options)
            .map(res => true);
    }
}