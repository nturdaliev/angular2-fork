export interface PropertyModel {
    TabName:string;
    PropertyName:string;
    PropertyID:number;
    Type:string;
    Length:number;
    Required:boolean;
    ListID:string;
    DefaultItemID:string;
    DefaultValue:string;
}