/**
 * Created by nursultan on 2/25/16.
 */
import {Component} from 'angular2/core';
import {Router} from 'angular2/router';
import {LoginService} from "./login.service";
import {LoginModel} from "./LoginModel";
import {HTTP_PROVIDERS} from "angular2/http";
import {Container} from "../Container";

@Component({
    selector: 'login-component',
    templateUrl: 'app/auth/login-component.html',
    styleUrls: ['app/auth/login.css'],
    providers: [LoginService, LoginModel, HTTP_PROVIDERS, Container]
})
export class LoginComponent {
    private router:Router;
    private loginService:LoginService;
    public errorOccurred;
    public loading:boolean;

    public loginModel:LoginModel;

    constructor(router:Router, loginService:LoginService) {
        this.router = router;
        this.loginModel = new LoginModel('', '');
        this.loginService = loginService;
    }

    onSubmit() {
        this.loading = true;
        this.loginService.authenticate(this.loginModel)
            .subscribe((response) => {
                    this.loading = false;
                    response = JSON.parse(response._body);
                    localStorage.setItem('token', response.token);
                    this.router.navigate(['Dashboard']);
                },
                (error)=> {
                    this.errorOccurred = true;
                    this.loading = false;
                });
    }
}