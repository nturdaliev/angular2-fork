/**
 * Created by nursultan on 3/1/16.
 */
import {RouterOutlet} from "angular2/router";
import {Directive} from "angular2/core";
import {ComponentInstruction} from "angular2/router";
import {PromiseWrapper} from "angular2/src/facade/promise";
import {ElementRef} from "angular2/core";
import {DynamicComponentLoader} from "angular2/core";
import {LoginComponent} from "./login.component";
import {Injectable} from "angular2/core";
import {Router} from "angular2/router";
import {JwtHelper} from "./jwt-helper";

@Directive({
    selector: 'logged-in-router-outlet',
    providers: [JwtHelper]
})
@Injectable()
export class LoggedInOutlet extends RouterOutlet {
    private parentRouter:Router;

    constructor(_elementRef:ElementRef, _loader:DynamicComponentLoader, _parentRouter:Router, nameAttr:string) {
        this.parentRouter = _parentRouter;
        super(_elementRef, _loader, _parentRouter, nameAttr);
    }


    activate(nextInstruction:ComponentInstruction):Promise<any> {
        let jwtHelper = new JwtHelper();
        if (this.parentRouter.lastNavigationAttempt !== '/login' && jwtHelper.isTokenExpired()) {
            this.parentRouter.navigate(['/Login']);
        }
        return super.activate(nextInstruction);
    }

    routerCanDeactivate(nextInstruction:ComponentInstruction):Promise<boolean> {

        return PromiseWrapper.resolve(true);
    }
}