import {Directive} from "angular2/core";
import {Input} from "angular2/core";
import {ElementRef} from "angular2/core";
import {AfterViewInit} from "angular2/core";
@Directive({
    selector: '[materialize-textarea]'
})
export class MaterializeTextarea implements AfterViewInit {

    @Input('materialize-textarea') materializeTextarea:any;

    constructor(private element:ElementRef) {

    }

    ngAfterViewInit():any {
        $(this.element.nativeElement).on('focus', function () {
            $(this).trigger('autoresize');
        });
        return true;
    }
}