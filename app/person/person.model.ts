/**
 * Created by nursultan on 3/4/16.
 */
export interface PersonModel {
    ID:string;
    PersonTypeCode:string;
    EntityType:string;
    PersonNo:string;
    Name:string;
}