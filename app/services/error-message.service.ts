import {Injectable} from "angular2/core";
@Injectable()
export class ErrorMessageService {
    private messages = {
        required: "Required",
        maxlength: "Length is longer than allowed",
        incorrectMailFormat: "Incorrect mail address"
    };

    getMessage(errorType):string {
        if (!this.messages[errorType]) {
            return "Invalid input"
        }
        return this.messages[errorType];
    }

    getErrorMessages(errors:any):string[] {
        var errorMessages = [], message:string;
        for (var property in errors) {
            message = this.getMessage(property);
            errorMessages.push(message);
        }
        return errorMessages;
    }
}