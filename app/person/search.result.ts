import {PersonModel} from "./person.model";
/**
 * Created by nursultan on 3/4/16.
 */
export interface SearchResult {
    Count:number;
    PageSize:number;
    Keyword:string;
    PageIndex:number;
    Result:PersonModel[];
}