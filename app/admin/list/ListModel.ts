import {ListItem} from "./ListItem";
/**
 * Created by nursultan on 3/2/16.
 */
export interface ListModel {
    ID?: string;
    ListName: string;
    BuiltIn: boolean;
    Items:ListItem[];
}