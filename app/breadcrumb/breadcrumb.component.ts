import {Component} from "angular2/core";
import {Router} from "angular2/router";
import {RouterLink} from "angular2/router";
import {BreadcrumbService} from "../services/breadcrumb.service";
import {Parameters} from "./Parameters";
/**
 * Created by nursultan on 4/8/16.
 */
@Component({
    selector: 'breadcrumb-component',
    templateUrl: 'app/breadcrumb/breadcrumb.component.html',
    styleUrls: ['app/breadcrumb/breadcrumb.css'],
    directives: [RouterLink]
})
export class BreadcrumbComponent {
    public breadcrumbsCollection:Array<string> = ['People'];

    public routes:Array<any>;
    public last:string;
    public title:string;

    constructor(private _breadcrumbService:BreadcrumbService) {
        _breadcrumbService.componentNameStream$.subscribe((parameters)=> {
            this.configBreadcrumb(parameters);
        });
    }

    private configBreadcrumb(newValue:Parameters):any {
        switch (newValue.key) {
            case 'People':
                this.routes = this.gerRoutesForPeople(newValue);
                this.last = newValue.profile.PersonNo;
                this.title = newValue.profile.Name;
                break;
            case 'Lists':
                this.routes = this.getRoutesForList(newValue);
                this.last = 'Lists';
                this.title = 'List Management';
                break;
            case 'ListItem':
                this.routes = this.getRoutesForListItem(newValue);
                this.last = newValue.last;
                this.title = 'List';
                break;
        }
    }

    private gerRoutesForPeople(object:Parameters):any {
        return [
            {RouterLink: ['/Dashboard', 'Home'], name: 'Home'},
            {RouterLink: ['PeopleList', {'personType': object.personType}], name: 'People List'}
        ];
    }

    private getRoutesForList(newValue:Parameters):Array<any> {
        return [
            {RouterLink: ['/Dashboard', 'Admin'], name: 'Admin'}
        ];
    }

    private getRoutesForListItem(newValue:Parameters):Array<any> {
        return [
            {RouterLink: ['/Dashboard', 'Admin'], name: 'Admin'},
            {RouterLink: ['/Dashboard', 'Lists'], name: 'Lists'},
        ];
    }
}