import {PersonTypeModel} from "./person-type.model";
import {TabModel} from "../people/sub-components/tab.model";
export interface NewPersonModel {
    PersonType:PersonTypeModel;
    Properties:TabModel[];
}