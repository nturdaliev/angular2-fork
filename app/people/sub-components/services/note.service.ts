import {Injectable} from "angular2/core";
import {SecuredService} from "../../../services/secured.service";
import {Http} from "angular2/http";
import {Container} from "../../../Container";
import {NoteModel} from "../model/note.model";
import {Router} from "angular2/router";
@Injectable()
export class NoteService extends SecuredService {

    constructor(public http:Http, public container:Container,public router:Router) {
        super(http, container,router);
    }

    get(personType:string, personID:string) {
        return this.http.get(this.getBaseUrl(personType, personID), this.options)
            .map(response => <NoteModel[]>response.json());
    }

    note(personType:string, personID:string, Id:string) {
        return this.http.get(this.getBaseUrl(personType, personID) + Id, this.options)
            .map(response => <NoteModel>response.json());
    }

    delete(personType:string, personID:string, ID:string):any {
        return this.http.delete(this.getBaseUrl(personType, personID) + ID, this.options)
            .map(response => {
                response
            });
    }

    post(personType:string, personID:string, newNote:NoteModel):any {
        var body = JSON.stringify(newNote);
        return this.http.post(this.getBaseUrl(personType, personID), body, this.options)
            .map(response => <NoteModel>response.json());
    }
    put(personType:string, personID:string, newNote:NoteModel):any {
        var body = JSON.stringify(newNote);
        return this.http.put(this.getBaseUrl(personType, personID) + newNote.ID, body, this.options)
            .map(response => <NoteModel>response.json());
    }

    private getBaseUrl(personType:string, personID:string):string {
        return this.container.baseUrl + '/people/' + personType + '/' + personID + '/notes/';
    }
}