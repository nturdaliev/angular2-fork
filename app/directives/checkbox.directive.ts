import {Directive, ElementRef, Input} from 'angular2/core';
import {StringService} from "../services/string.service";
import {AfterViewInit} from "angular2/core";
@Directive({
    selector: '[materialize-checkbox]',
})
export class CheckboxDirective implements AfterViewInit {
    @Input('materialize-checkbox') value:string;

    constructor(private el:ElementRef, private stringService:StringService) {
    }

    ngAfterViewInit():any {
        jQuery(this.el.nativeElement).attr('id', this.stringService.normalize(this.value));
        return true;
    }
}