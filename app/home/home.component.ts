/**
 * Created by nursultan on 2/27/16.
 */
import {Component} from 'angular2/core'

@Component(
    {
        selector: 'home-component',
        templateUrl: 'app/home/home-component.html'
    }
)
export class HomeComponent {
}