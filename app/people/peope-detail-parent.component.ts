import {Component} from "angular2/core";
import {LoggedInOutlet} from "../auth/loggedin-outlet";
import {RouteConfig} from "angular2/router";
import {PeopleSummaryComponent} from "./sub-components/people-summary.component";
import {RouteParams} from "angular2/router";
import {OnInit} from "angular2/core";
import {PersonService} from "../person/person.service";
import {HTTP_PROVIDERS} from "angular2/http";
import {Container} from "../Container";
import {PageModel} from "./page-model";
import {PeopleProfileComponent} from "./sub-components/people-profile.component";
import {PeopleNotesListComponent} from "./sub-components/people-notes-list.component";
import {ROUTER_DIRECTIVES} from "angular2/router";
import {NoteService} from "./sub-components/services/note.service";
import {PageService} from "./sub-components/services/page.service";
import {AfterContentInit} from "angular2/core";
import {Injector} from "angular2/core";
import {BreadcrumbComponent} from "../breadcrumb/breadcrumb.component";
import {BreadcrumbService} from "../services/breadcrumb.service";
@Component({
    templateUrl: 'app/people/people-detail-parent.component.html',
    styleUrls: ['app/people/people-detail.css'],
    directives: [LoggedInOutlet, ROUTER_DIRECTIVES, BreadcrumbComponent],
    providers: [PersonService, HTTP_PROVIDERS, Container, NoteService, PageService]
})
@RouteConfig([
    {path: "/profile", name: "PeopleProfile", component: PeopleProfileComponent},
    {path: "/notes", name: "NoteList", component: PeopleNotesListComponent},
])
export class PeopleDetailParentComponent implements OnInit {
    public person:any;
    public pages:PageModel[];
    private personID:string;
    private personType:string;
    private selectedPage:PageModel;

    constructor(private routeParams:RouteParams,
                private pageService:PageService,
                private _breadcrumbService:BreadcrumbService,
                private _personService:PersonService) {

    }

    ngOnInit():any {
        this.personID = this.routeParams.get('personID');
        this.personType = this.routeParams.get('personType');
        this.getProfile();
        this.configPages();
        this.configSelectedPage();
        return undefined;
    }

    private configPages():void {
        this.pages = this.pageService.get(this.personType, this.personID);
    }

    private getProfile():void {
        this._personService.getProfile(this.personType, this.personID).subscribe(profile=> {
            let newValue = {key: 'People', personID: this.personID, personType: this.personType, profile: profile};
            this._breadcrumbService.broadcastComponentChange(newValue);
        });
    }

    private configSelectedPage():void {
        var pageType = this.getPageType();
        for (var index = 0; index < this.pages.length; index++) {
            if (this.pages[index].title.toLocaleLowerCase() == pageType.toLocaleLowerCase()) {
                this.selectedPage = this.pages[index];
            }
        }
    }

    private getPageType():string {
        var path = window.location.pathname;
        return path.substring(path.lastIndexOf('/') + 1);
    }
}