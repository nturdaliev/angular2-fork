import {PropertyModel} from "./properyt.model";
export interface TabModel {
    PropertyID:number;
    Property:PropertyModel;
    Value:any;
    ListItemID:string;
}