import {Container} from "../Container";
import {Http} from "angular2/http";
import {Headers} from "angular2/http";
import {RequestOptions} from "angular2/http";
import {Injectable} from "angular2/core";
import {Response} from "angular2/http";
import {Observable} from "rxjs/Observable";
import {Router} from "angular2/router";
@Injectable()
export class SecuredService {
    public options:RequestOptions;

    constructor(public http:Http, public container:Container, public router:Router) {
        this.http = http;
        this.container = container;
        let headers = new Headers({
            'Authorization': localStorage.getItem('token'),
            'Content-Type': 'application/json'
        });
        this.options = new RequestOptions({headers: headers});
    }


    protected handleError(error:Response) {
        if (error.status == 401) {
            this.router.navigate(['/Login']);
        }
        return Observable.throw(error);
    }
}