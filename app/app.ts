import {bootstrap}    from 'angular2/platform/browser';
import {provide} from 'angular2/core';
import 'rxjs/Rx';
import {ROUTER_PROVIDERS,LocationStrategy,HashLocationStrategy } from 'angular2/router';
import {AppComponent} from './app.component'
import {InputService} from "./services/input.service";
import {StringService} from "./services/string.service";
bootstrap(AppComponent, [ROUTER_PROVIDERS, InputService, StringService, provide(LocationStrategy, {useClass: HashLocationStrategy})]);