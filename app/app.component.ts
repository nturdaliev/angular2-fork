/**
 * Created by nursultan on 2/25/16.
 */
import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouterLink } from 'angular2/router';
import {LoginComponent} from './auth/login.component';
import {DashboardComponent} from './dashboard.component';
import {LoggedInOutlet} from "./auth/loggedin-outlet";
import {LoaderService} from "./loader.service";
import {Subscription} from "rxjs/Subscription";
import {AfterViewInit} from "angular2/core";
import {enableProdMode} from 'angular2/core';
enableProdMode();
@RouteConfig([
    {
        path: '/dashboard/...',
        name: 'Dashboard',
        component: DashboardComponent,
        useAsDefault: true
    }
])
@RouteConfig([
    {
        path: '/login',
        name: 'Login',
        component: LoginComponent
    }
])
@Component({
    selector: 'app-component',
    template: `<router-outlet></router-outlet>
                <div class="loader" *ngIf="loading">Loading...</div>`,
    directives: [ROUTER_DIRECTIVES],
    providers: [ROUTER_PROVIDERS, LoaderService]
})
export class AppComponent implements AfterViewInit{
    ngAfterViewInit():any {
        this.loading = false;
        this.subscription = this._loaderService.loadingStream$.subscribe(
            isLoading => {
                this.loading = isLoading;
            }
        );
        return undefined;
    }
    subscription:Subscription;
    loading:boolean;

    constructor(private _loaderService:LoaderService) {
    }

}