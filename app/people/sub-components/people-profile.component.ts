import {Component} from "angular2/core";
import {PersonService} from "../../person/person.service";
import {OnInit} from "angular2/core";
import {RouteParams} from "angular2/router";
import {ProfileModel} from "./profile-model";
import {TabDirective} from "../../directives/tab.directive";
import {InputService} from "../../services/input.service";
import {NgSwitchDefault} from "angular2/common";
import {NgSwitchWhen} from "angular2/common";
import {NgSwitch} from "angular2/common";
import {DateDirective} from "../../directives/date.directive";
import {CheckboxDirective} from "../../directives/checkbox.directive";
import {StringService} from "../../services/string.service";
import {NormalizedForDirective} from "../../directives/normalized-for.directive";
import {NormalizedIdDirective} from "../../directives/normalized-id.directive";
import {SelectDirective} from "../../directives/select.directive";
import {ListService} from "../../admin/list/list.service";
import {ListItem} from "../../admin/list/ListItem";
import {FormInputComponent} from "../form-input.component";
import {FORM_PROVIDERS} from "angular2/common";
import {FORM_DIRECTIVES} from "angular2/common";
import {SecuredService} from "../../services/secured.service";
import {PropertyControlService} from "../property-control.service";
import {DynamicFormComponent} from "../dynamic-form.component";
import {Injector} from "angular2/core";
import {PeopleDetailParentComponent} from "../peope-detail-parent.component";
import {RouterLink} from "angular2/router";
import {NewPersonFormComponent} from "../new-person-form.component";
@Component({
    selector: 'people-profile',
    templateUrl: 'app/people/sub-components/people-profile.component.html',
    providers: [
        PersonService,
        ListService,
        FORM_PROVIDERS,
        SecuredService],
    directives: [
        TabDirective,
        DynamicFormComponent,
        NewPersonFormComponent,
        RouterLink,
        FORM_DIRECTIVES]
})
export class PeopleProfileComponent implements OnInit {
    private profileModel:ProfileModel;
    private tabNames:string[];
    public inputs:string[];
    private id:string;
    private personType:string;
    private submitting:boolean;


    ngOnInit():any {
        var parentComponent = this.injector.get(PeopleDetailParentComponent);
        this.id = parentComponent.personID;
        this.personType = parentComponent.personType;
        this.personService.getProfile(this.personType, this.id)
            .subscribe(profileModel => {
                this.profileModel = profileModel;
                this.prepareTabs();
            });
        this.inputs = this.inputService.getInputs();
        return true;
    }

    constructor(private personService:PersonService,
                private injector:Injector,
                public inputService:InputService) {
        this.submitting = false;
    }


    private prepareTabs():void {
        let tabNames = [];
        this.profileModel.Properties.forEach((tabModel, index) => {
            tabNames[tabModel.Property.TabName] = true;
            if (tabModel.Property.Type == 'Checkbox' && tabModel.Value) {
                if (tabModel.Value.toLocaleLowerCase() == 'false') {
                    this.profileModel.Properties[index].Value = false;
                } else if (tabModel.Value.toLocaleLowerCase() == 'true') {
                    this.profileModel.Properties[index].Value = true;
                }

            } else if (tabModel.Property.Type == 'Date') {
                this.profileModel.Properties[index].Value = moment(this.profileModel.Properties[index].Value).format('YYYY-MM-DD');
            }

        });
        this.tabNames = Object.keys(tabNames);
    }
}