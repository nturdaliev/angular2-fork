import {Component} from "angular2/core";
import {LoggedInOutlet} from "../auth/loggedin-outlet";
import {PersonService} from "../person/person.service";
import {RouteConfig} from "angular2/router";
import {PeopleListComponent} from "./people-list.component";
import {PeopleDetailParentComponent} from "./peope-detail-parent.component";
import {BreadcrumbService} from "../services/breadcrumb.service";
import {PersonCriteria} from "../person/person.criteria";
@Component({
    template: `
    <logged-in-router-outlet></logged-in-router-outlet>
    `,
    directives: [LoggedInOutlet],
    providers: [PersonService]
})
@RouteConfig([
    {path: '/:personType', name: 'PeopleList', component: PeopleListComponent},
    {path: '/:personType/:personID/...', name: 'PeopleDetailParent', component: PeopleDetailParentComponent}
])
export class PeopleComponent {
    public personCriteria:PersonCriteria;

    constructor() {
        this.personCriteria = <PersonCriteria>{
            Keyword: '',
            PersonType: null,
            PageSize: 10,
            PageIndex: 0
        }
    }
}