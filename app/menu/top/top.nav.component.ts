/**
 * Created by nursultan on 2/27/16.
 */
import {Component} from 'angular2/core'
import {LoginService} from "../../auth/login.service";
import {Http} from "angular2/http";
import {Router} from "angular2/router";
import {AfterContentInit} from "angular2/core";
import {SearchService} from "../../services/search.service";

@Component({
    selector: 'top-nav-component',
    templateUrl: 'app/menu/top/top-nav-component.html',
    styleUrls: ['app/menu/top/top.css'],
    providers: [LoginService]
})
export class TopNavComponent implements AfterContentInit {

    constructor(private router:Router) {
    }

    ngAfterContentInit():any {
        jQuery('.dropdown-button-top').dropdown({
                inDuration: 300,
                outDuration: 225,
                hover: true,
                gutter: 0,
                belowOrigin: true,
                alignment: 'right'
            }
        );
    }

    onLogoutClick() {
        localStorage.removeItem('token');
        this.router.navigate(['Login']);
    }
}