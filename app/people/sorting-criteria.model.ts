/**
 * Created by nursultan on 3/14/16.
 */
export interface SortingCriteria {
    property:string;
    direction:string;
}