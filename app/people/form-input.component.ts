///<reference path="../../node_modules/angular2/src/core/linker/interfaces.d.ts"/>
import {Component} from "angular2/core";
import {Input} from "angular2/core";
import {SelectDirective} from "../directives/select.directive";
import {CheckboxDirective} from "../directives/checkbox.directive";
import {TabDirective} from "../directives/tab.directive";
import {NormalizedForDirective} from "../directives/normalized-for.directive";
import {NormalizedIdDirective} from "../directives/normalized-id.directive";
import {NgSwitch} from "angular2/common";
import {NgSwitchDefault} from "angular2/common";
import {NgSwitchWhen} from "angular2/common";
import {DateDirective} from "../directives/date.directive";
import {ListService} from "../admin/list/list.service";
import {ListItem} from "../admin/list/ListItem";
import {AfterViewInit} from "angular2/core";
import {FORM_DIRECTIVES} from "angular2/common";
import {FORM_PROVIDERS} from "angular2/common";
import {ControlGroup} from "angular2/common";
import {PropertyControlService} from "./property-control.service";
import {OnInit} from "angular2/core";
import {TabModel} from "./sub-components/tab.model";
import {MyCounterDirective} from "../directives/my-counter.directive";
@Component({
    selector: 'form-input',
    templateUrl: 'app/people/form-input.component.html',
    directives: [SelectDirective,
        CheckboxDirective,
        DateDirective,
        NormalizedForDirective,
        NormalizedIdDirective,
        MyCounterDirective,
        NgSwitch,
        NgSwitchDefault,
        NgSwitchWhen,
        FORM_DIRECTIVES],
    providers: [FORM_PROVIDERS]
})
export class FormInputComponent implements AfterViewInit {
    @Input('property') property:TabModel;
    @Input('form') form:any;
    public list:any;

    constructor(private listService:ListService) {
    }

    ngAfterViewInit():any {
        if (this.property.Property.Type == 'List') {
            this.listService.getListItems(this.property.Property.ListID)
                .subscribe(res => {
                    this.list = res;
                });
        }
        return true;
    }
}