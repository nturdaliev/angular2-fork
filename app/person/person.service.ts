import {Http} from "angular2/http";
import {Injectable} from "angular2/core";
import {Container} from "../Container";
import {RequestOptions} from "angular2/http";
import {Headers} from "angular2/http";
import {PersonCriteria} from "./person.criteria";
import {PersonModel} from "./person.model";
import {Observable} from "rxjs/Observable";
import {Response} from "angular2/http";
import {ProfileModel} from "../people/sub-components/profile-model";
import {SecuredService} from "../services/secured.service";
import {NewPersonModel} from "./new-person.model";
import {Router} from "angular2/router";
/**
 * Created by nursultan on 3/4/16.
 */
@Injectable()
export class PersonService extends SecuredService {

    constructor(http:Http, container:Container,router:Router) {
        super(http, container,router);
    }

    findByKeywordAndPersonType(personCriteria:PersonCriteria) {
        let body = JSON.stringify(personCriteria);
        return this.http.post(this.container.baseUrl + '/search/people', body, this.options)
            .map(res =><PersonModel[]> res.json())
            .catch(this.handleError);
    }

    getProfile(personType:string, id:string) {
        return this.http.get(this.container.baseUrl + '/people/' + personType + '/' + id + '/profile', this.options)
            .map(res =><ProfileModel> res.json())
            .catch(this.handleError);
    }

    updateProfile(personType:string, id:string, personModel:ProfileModel) {
        let body = JSON.stringify(personModel);
        return this.http.put(this.container.baseUrl + '/people/' + personType + '/' + id + '/profile', body, this.options)
            .map(res => <ProfileModel>res.json());
    }

    delete(person:PersonModel) {
        return this.http.delete(this.container.baseUrl + '/people/' + person.PersonTypeCode.toLocaleLowerCase() + '/' + person.ID + '/profile', this.options)
            .map(res => <ProfileModel>res.json());
    }

    newPerson(personType:string) {
        return this.http.get(this.container.baseUrl + '/people/' + personType + '/newtemplate', this.options)
            .map(response =><NewPersonModel>response.json());
    }


    create(personType:String, profileModel:NewPersonModel):any {
        var body = JSON.stringify(profileModel);
        return this.http.post(this.container.baseUrl + '/people/' + personType, body, this.options)
            .map(response => <ProfileModel>response.json());
    }
}