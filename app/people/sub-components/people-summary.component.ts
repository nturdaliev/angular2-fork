import {Component} from "angular2/core";
import {HTTP_PROVIDERS} from "angular2/http";
import {PersonService} from "../../person/person.service";
@Component({
    selector: 'people-summary',
    templateUrl:'app/people/sub-components/people-summary.component.html',
    providers:[HTTP_PROVIDERS,PersonService]
})
export class PeopleSummaryComponent {

}