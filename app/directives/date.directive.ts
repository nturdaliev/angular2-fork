import {Directive} from "angular2/core";
import {ElementRef} from "angular2/core";
import {Input} from "angular2/core";
import {AfterViewInit} from "angular2/core";
import {EventEmitter} from "angular2/core";
@Directive({
    selector: '[materialize-date]'
})
export class DateDirective implements AfterViewInit {
    @Input('materialize-date') element:string;

    constructor(private el:ElementRef) {
    }

    ngAfterViewInit():any {
        var value = jQuery(this.el.nativeElement).data('value');
        jQuery(this.el.nativeElement).val(value);
    }
}