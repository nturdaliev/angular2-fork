import {Directive, ElementRef, Input} from 'angular2/core';
import {StringService} from "../services/string.service";
import {AfterViewInit} from "angular2/core";
@Directive({
    selector: '[normalized-for]',
})
export class NormalizedForDirective implements AfterViewInit {
    @Input('normalized-for') element:string;

    constructor(private el:ElementRef, private stringService:StringService) {
    }

    ngAfterViewInit():any {
        jQuery(this.el.nativeElement).attr('for', this.stringService.normalize(this.element));
    }
}