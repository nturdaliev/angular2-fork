import {Component} from "angular2/core";
import {HTTP_PROVIDERS} from "angular2/http";
import {ListListComponent} from "./list/list-list.component";
import {ROUTER_DIRECTIVES} from "angular2/router";
/**
 * Created by nursultan on 3/2/16.
 */
@Component({
    selector: 'admin-component',
    templateUrl: 'app/admin/admin.html',
    directives: [ListListComponent,ROUTER_DIRECTIVES],
    providers: [HTTP_PROVIDERS]
})
export class AdminComponent {
}