import {Injectable} from "angular2/core";
import {FormBuilder} from "angular2/common";
import {InputProperty} from "./input.property.class";
import {PropertyModel} from "./sub-components/properyt.model";
import {TabModel} from "./sub-components/tab.model";
import {Validators} from "angular2/common";
import {GlobalValidator} from "../form/global.validator";
@Injectable()
export class PropertyControlService {
    constructor(private formBuilder:FormBuilder) {
    }

    toControlGroup(tabModels:TabModel[]) {
        let group = {};

        tabModels.forEach(tabModel => {
            var validators = [];
            if (tabModel.Property.Required) {
                validators.push(Validators.required);
            }
            if (tabModel.Property.Length > 0) {
                validators.push(Validators.maxLength(tabModel.Property.Length));
            }
            if (tabModel.Property.Type == 'Email') {
                validators.push(GlobalValidator.mailFormat);
            }
            group[tabModel.PropertyID] = [tabModel.Value || '', Validators.compose(validators)];
        });
        return this.formBuilder.group(group);
    }
}