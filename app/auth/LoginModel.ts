import {Injectable} from "angular2/core";
/**
 * Created by nursultan on 3/1/16.
 */
@Injectable()
export class LoginModel {
    constructor(public username:string, public password:string) {
    }
}