/**
 * Created by nursultan on 3/2/16.
 */
export interface ListItem {
    ID:string;
    Code:string;
    Title:string;
}