import {Directive} from "angular2/core";
import {ElementRef} from "angular2/core";
import {AfterViewInit} from "angular2/core";
import {Input} from "angular2/core";
@Directive({
    selector: '[dropdown]'
})
export class DropDownButton implements AfterViewInit {

    @Input('dropdown') dropdown:number;
    constructor(private el:ElementRef) {

    }

    ngAfterViewInit():any {
        jQuery(this.el.nativeElement).dropdown({
                inDuration: 300,
                outDuration: 225,
                hover: true,
                gutter: -10,
                belowOrigin: true,
                alignment: 'right'
            }
        );
        return undefined;
    }
}