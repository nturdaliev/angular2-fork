/**
 * Created by nursultan on 2/27/16.
 */
import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';
import {LeftMenuService} from "./left-menu.service";
import {OnInit} from "angular2/core";
import {HTTP_PROVIDERS} from "angular2/http";
import {Container} from "../../Container";
@Component({
    selector: 'left-nav-component',
    templateUrl: 'app/menu/left/left-nav-component.html',
    styleUrls: ['app/menu/left/left.css'],
    directives: [ROUTER_DIRECTIVES],
    providers: [LeftMenuService, HTTP_PROVIDERS, Container]

})
export class LeftNavComponent implements OnInit {
    public leftMenu:any;
    public rootMenuName:string;
    public activeMenuIndex = -1;
    public error:any;

    ngOnInit():any {
        this.leftMenuService.getMenu(this.rootMenuName)
            .subscribe(
                menuArray => this.leftMenu = menuArray,
                error=> error
            )
        ;
        return true;
    }

    onClick(index) {
        this.activeMenuIndex = index;
    }

    constructor(private leftMenuService:LeftMenuService) {
        this.rootMenuName = 'Home';
    }
}