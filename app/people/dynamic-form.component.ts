import {Component} from "angular2/core";
import {OnInit} from "angular2/core";
import {PropertyControlService} from "./property-control.service";
import {ControlGroup} from "angular2/common";
import {Input} from "angular2/core";
import {TabModel} from "./sub-components/tab.model";
import {FormInputComponent} from "./form-input.component";
import {TabDirective} from "../directives/tab.directive";
import {PersonService} from "../person/person.service";
import {RouteParams} from "angular2/router";
import {ProfileModel} from "./sub-components/profile-model";
import {PeopleDetailParentComponent} from "./peope-detail-parent.component";
import {Injector} from "angular2/core";
import {ErrorMessageService} from "../services/error-message.service";
@Component({
    selector: 'dynamic-form',
    templateUrl: 'app/people/dynamic-form.component.html',
    directives: [FormInputComponent, TabDirective],
    providers: [PropertyControlService, ErrorMessageService]
})
export class DynamicFormComponent implements OnInit {
    @Input('profileModel') profileModel:ProfileModel;
    @Input('tabNames') tabNames:TabModel[] = [];

    public form:ControlGroup;
    public id:string;
    public personType:string;
    public submitting:boolean;

    constructor(public propertyControlService:PropertyControlService,
                private personService:PersonService,
                private errorMessageService:ErrorMessageService,
                private injector:Injector) {

    }

    ngOnInit() {
        var parentComponent = this.injector.get(PeopleDetailParentComponent);
        this.id = parentComponent.personID;
        this.personType = parentComponent.personType;
        this.submitting = false;
        this.form = this.propertyControlService.toControlGroup(this.profileModel.Properties);
    }

    getFormControls() {
        let controls = [];
        for (var control in this.form.controls) {
            var property = this.getProperty(control);
            if (property && !this.form.controls[control].valid) {
                var errors = this.errorMessageService.getErrorMessages(this.form.controls[control].errors);
                controls.push({property: property, errors: errors});
            }
        }
        return controls;
    }

    onSubmit() {
        this.submitting = true;
        this.personService.updateProfile(this.personType, this.id, this.profileModel)
            .subscribe(res => {
                this.submitting = false;
                Materialize.toast("Updated", 2000);
            }, error=> {
                this.submitting = false;
                Materialize.toast(error._body, 5000);
            });
    }

    private getProperty(propertyID):TabModel {
        for (var tabModel in this.profileModel.Properties) {
            if (this.profileModel.Properties[tabModel].PropertyID == propertyID) {
                return this.profileModel.Properties[tabModel];
            }
        }
        return null;
    }
}