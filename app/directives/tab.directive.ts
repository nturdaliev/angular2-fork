import {Directive} from "angular2/core";
import {ElementRef} from "angular2/core";
import {Input} from "angular2/core";
@Directive({
    selector: '[materialize-tab]'
})
export class TabDirective {
    @Input('materialize-tab') element:string;

    constructor(private el:ElementRef) {
    }

    ngAfterViewInit():any {
        jQuery(this.el.nativeElement).tabs();
    }
}