import {PersonTypeModel} from "../../person/person-type.model";
import {TabModel} from "./tab.model";
export interface ProfileModel {
    ID:string;
    PersonNo:string;
    Name:string;
    PersonType:PersonTypeModel;
    DateCreated:string;
    DateModified:string;
    Locked:boolean;
    Username:string;
    Properties:TabModel[]
}