/**
 * Created by nursultan on 3/2/16.
 */

export interface MenuModel {
    PageTitle: string;
    RouterLink:string,
    Icon: string;
}