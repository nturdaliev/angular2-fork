/**
 * Created by nursultan on 3/4/16.
 */
export interface PersonCriteria {
    Keyword:string;
    PersonType:string;
    PageSize:number;
    PageIndex:number;
}