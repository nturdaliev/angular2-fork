import {Injectable} from "angular2/core";
import {Subject} from "rxjs/Subject";
@Injectable()
export class LoaderService {
    private _loading = new Subject<boolean>();

    public loadingStream$ = this._loading.asObservable();

    broadcastLoadingChange(isLoading:boolean) {
        this._loading.next(isLoading);
    }
}