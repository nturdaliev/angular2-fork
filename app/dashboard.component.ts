import {Component} from 'angular2/core';
import {RouteConfig, RouterOutlet} from 'angular2/router';
import {AfterContentInit} from 'angular2/core';

import {LeftNavComponent} from './menu/left/left.nav.component';
import {TopNavComponent} from './menu/top/top.nav.component';
import {HomeComponent} from './home/home.component';
import {Router} from "angular2/router";
import {LoggedInOutlet} from "./auth/loggedin-outlet";
import {ClassListComponent} from "./classes/class-list.component";
import {ReportListComponent} from "./reports/report-list.component";
import {AdminComponent} from "./admin/admin.component";
import {ListListComponent} from "./admin/list/list-list.component";
import {ListItemComponent} from "./admin/list/list-item.component";
import {PeopleComponent} from "./people/people.component";
import {SearchService} from "./services/search.service";
import {BreadcrumbService} from "./services/breadcrumb.service";
import {MaterializeTextarea} from "./directives/textarea.directive";
@RouteConfig([
    {path: '/', name: 'Home', component: HomeComponent, useAsDefault: true},
    {path: '/classes', name: 'Classes', component: ClassListComponent},
    {path: '/reports', name: 'Reports', component: ReportListComponent},
    {path: '/admin', name: 'Admin', component: AdminComponent},
    {path: '/lists', name: 'Lists', component: ListListComponent},
    {path: '/lists/:id', name: 'List', component: ListItemComponent},
    {path: '/people/...', name: 'People', component: PeopleComponent},
])
@Component({
    selector: 'dashboard-component',
    templateUrl: 'app/dashboard-component.html',
    styleUrls: ['app/dashboard.css'],
    providers: [SearchService, BreadcrumbService],
    directives: [RouterOutlet, LeftNavComponent, TopNavComponent, HomeComponent, LoggedInOutlet, MaterializeTextarea]
})
export class DashboardComponent implements AfterContentInit {

    private token:string;

    constructor() {
        this.token = localStorage.getItem('token');
    }

    ngAfterContentInit():any {
        this.initLeftNavigation();
        return true;
    }

    initLeftNavigation() {
        jQuery(() => {
            jQuery('.button-collapse').sideNav({'edge': 'left'});
            jQuery(".dropdown-button").dropdown();
            jQuery('.collapsible').collapsible({
                accordion: false
            });
        });
    }
}