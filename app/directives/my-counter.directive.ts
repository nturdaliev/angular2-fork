import {Directive} from "angular2/core";
import {Input} from "angular2/core";
import {ElementRef} from "angular2/core";
import {AfterViewInit} from "angular2/core";
@Directive({
    selector: '[my-counter]'
})
export class MyCounterDirective implements AfterViewInit {

    @Input('my-counter') length:number;

    constructor(private el:ElementRef) {
    }

    ngAfterViewInit():any {
        if (this.length > 0) {
            $(this.el.nativeElement).attr('length', this.length);
            jQuery(this.el.nativeElement).characterCounter();
        }
        return undefined;
    }
}