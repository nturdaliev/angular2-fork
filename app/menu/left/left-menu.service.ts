import {Injectable} from "angular2/core";
import {Container} from "../../Container";
import {Http} from "angular2/http";
import {Headers} from "angular2/http";
import {RequestOptions} from "angular2/http";
import {MenuModel} from "./menu-model";
import {Response} from "angular2/http";
import {Observable} from "rxjs/Observable";
import {SecuredService} from "../../services/secured.service";
import {Router} from "angular2/router";

/**
 * Created by nursultan on 3/2/16.
 */
@Injectable()
export class LeftMenuService extends SecuredService {
    constructor(public http:Http, public container:Container,public router:Router) {
        super(http, container,router);
    }

    getMenu(rootMenu:string) {

        let result = [{
            "PageTitle": "Students",
            "RouterLink": ['People', 'PeopleList', {personType: 'st'}],
            "Icon": "&#xE853;"
        }, {
            "PageTitle": "Staff",
            "RouterLink": ['People', 'PeopleList', {personType: 'emp'}],
            "Icon": "&#xE853;"
        }, {
            "PageTitle": "Classes",
            "RouterLink": ['Classes'],
            "Icon": "&#xE916;"
        }, {
            "PageTitle": "Reports",
            "RouterLink": ['Reports'],
            "Icon": "&#xE6E1;"
        }, {
            "PageTitle": "Admin",
            "RouterLink": ['Admin'],
            "Icon": "&#xE8B8;"
        }];

        return this.http.get(this.container.baseUrl + '/menu/' + rootMenu, this.options)
            .map(res => result)
            .catch(this.handleError);
    }
}