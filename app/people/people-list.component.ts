import {Component} from "angular2/core";
import {PersonCriteria} from "../person/person.criteria";
import {PersonService} from "../person/person.service";
import {OnInit} from "angular2/core";
import {RouterLink} from "angular2/router";
import {Container} from "../Container";
import {HTTP_PROVIDERS} from "angular2/http";
import {RouteParams} from "angular2/router";
import {PaginatorModel} from "../paginator/paginator.model";
import {SelectDirective} from "../directives/select.directive";
import {SortingCriteria} from "./sorting-criteria.model";
import {NewPersonModel} from "../person/new-person.model";
import {FormInputComponent} from "./form-input.component";
import {ListService} from "../admin/list/list.service";
import {NewPersonFormComponent} from "./new-person-form.component";
import {SearchService} from "../services/search.service";
import {Subscription} from "rxjs/Subscription";
import {LoaderService} from "../loader.service";
import {PersonModel} from "../person/person.model";
import {Injector} from "angular2/core";
import {PeopleComponent} from "./people.component";
@Component({
    selector: 'people-list',
    templateUrl: 'app/people/people-list.component.html',
    styleUrls: ['app/people/people-list.css'],
    providers: [PersonService, HTTP_PROVIDERS, Container, ListService],
    directives: [
        RouterLink,
        SelectDirective,
        NewPersonFormComponent,
        FormInputComponent]
})
export class PeopleListComponent implements OnInit {
    public searchResult:any;
    public personCriteria:PersonCriteria;
    public paginatorModel:PaginatorModel;
    public sortingCriteria:SortingCriteria;
    public newPerson:NewPersonModel;
    private peopleComponent:PeopleComponent;
    showCount = [10, 25, 50, 100];
    subscription:Subscription;
    private tabNames:string[];

    constructor(private _personService:PersonService,
                private routeParams:RouteParams,
                private _searchService:SearchService,
                private _loaderService:LoaderService,
                private injector:Injector) {
        this.peopleComponent = this.injector.get(PeopleComponent);
        this.initPersonCriteria();
        this.subscription = this._searchService.searchTextStream$.subscribe(
            text => {
                this.personCriteria.Keyword = text;
                this.searchPeopleAndConfig();
            }
        );
    }


    ngOnInit():any {
        this.tabNames = [];
        this.searchPeopleAndConfig();
        this.configSortingCriteria('Name', 'Random');
        return true;
    }

    configSortingCriteria(currentProperty, direction) {
        this.sortingCriteria = {
            property: currentProperty,
            direction: direction
        };
    }

    deletePerson(person:PersonModel) {
        this._personService.delete(person)
            .subscribe(response => {
                console.log(response);
            });
    }

    paginatorClick(pageIndex):void {
        this.personCriteria.PageIndex = pageIndex;
        this.searchPeopleAndConfig();
    }

    preparePagination():void {
        this.paginatorModel = this.createPaginatorFromSearchResult();

        let startingIndex = this.paginatorModel.currentIndex - 3;
        if (startingIndex < 0) {
            startingIndex = 0;
        }
        let endingIndex = this.paginatorModel.currentIndex + 3;
        if (endingIndex >= this.paginatorModel.pageCount) {
            endingIndex = this.paginatorModel.pageCount - 1;
        }
        for (let index = startingIndex; index <= endingIndex; index++) {
            this.paginatorModel.indexList.push(index);
        }
        this.paginatorModel.lastIndex = this.paginatorModel.pageCount - 1;

    }

    createPaginatorFromSearchResult():PaginatorModel {
        return <PaginatorModel> {
            count: this.searchResult.Count,
            currentIndex: this.searchResult.PageIndex,
            pageSize: this.searchResult.PageSize,
            pageCount: Math.floor(this.searchResult.Count / this.searchResult.PageSize),
            lastIndex: 1,
            indexList: []
        };
    }

    onShowChange(value) {
        this.personCriteria.PageIndex = 0;
        this.personCriteria.PageSize = +value;
        this.searchPeopleAndConfig();
    }

    searchPeopleAndConfig():void {
        this._loaderService.broadcastLoadingChange(true);
        this._personService.findByKeywordAndPersonType(this.personCriteria)
            .subscribe(res => {
                this.searchResult = res;
                this.preparePagination();
                this.orderBy(this.sortingCriteria.property, this.sortingCriteria.direction);
                this._loaderService.broadcastLoadingChange(false);
            });
    }

    orderBy(property, direction) {
        if (!direction) {
            direction = 'asc';
            if (this.sortingCriteria.property == property) {
                if (this.sortingCriteria.direction !== 'desc') {
                    direction = 'desc';
                }
            }
        }
        this.searchResult.Result.sort((a, b)=> {
            return (direction == 'asc') ? a[property].localeCompare(b[property]) : b[property].localeCompare(a[property]);
        });
        this.configSortingCriteria(property, direction);
    }

    onNewPersonClick() {
        let personType = this.routeParams.get('personType');
        this._personService.newPerson(personType)
            .subscribe(response => {
                    this.newPerson = response;
                    this.prepareTabs();
                    jQuery('#modal-new-person').openModal();
                },
                error => Materialize.toast('An error occurred', 3000));
    }

    onChange(event) {
        this._searchService.broadcastTextChange(event.target.value);
    }

    prepareTabs():void {
        let tabNames = [];
        this.newPerson.Properties.forEach((tabModel) => {
            tabNames[tabModel.Property.TabName] = true;
        });
        this.tabNames = Object.keys(tabNames);
    }

    onModalCancel() {
        jQuery('#modal-new-person').closeModal();
    }

    initPersonCriteria():void {
        let personType = this.routeParams.get('personType');
        this.personCriteria = this.peopleComponent.personCriteria;
        this.personCriteria.PersonType = personType;
    }
}