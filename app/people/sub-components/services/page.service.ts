import {PageModel} from "../../page-model";
export class PageService {

    get(personType:string, personID:string) {
        return <PageModel[]>[{
            title: 'Profile',
            icon: '&#xE851;',
            RouterLink: ['PeopleDetailParent', {
                personType: personType, personID: personID
            }, 'PeopleProfile']
        },
            {
                title: 'Notes',
                icon: '&#xE06F;',
                RouterLink: ['PeopleDetailParent', {
                    personType: personType, personID: personID
                }, 'NoteList']
            }
        ];
    }
}