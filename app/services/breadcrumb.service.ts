import {Injectable} from "angular2/core";
import {BehaviorSubject} from "rxjs/Rx";
import {ReplaySubject} from "rxjs/Rx";
import {Parameters} from "../breadcrumb/Parameters";
@Injectable()
export class BreadcrumbService {

    private _componentName = new ReplaySubject<Parameters>(1);

    public componentNameStream$ = this._componentName.asObservable();

    broadcastComponentChange(newValue:Parameters) {
        this._componentName.next(newValue);
    }
}