import {ProfileModel} from "../people/sub-components/profile-model";
/**
 * Created by nursultan on 4/8/16.
 */
export interface Parameters {
    key:string
    profile?:ProfileModel;
    personType?:string;
    last?:string;
}