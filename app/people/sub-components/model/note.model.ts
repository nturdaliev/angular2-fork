export interface NoteModel {
    ID?:string,
    NoteDate?:string;
    CreatedBy?:Date;
    Note:string;
}