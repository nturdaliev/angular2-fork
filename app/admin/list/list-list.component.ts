import {Component} from "angular2/core";
import {ListService} from "./list.service";
import {OnInit} from "angular2/core";
import {Container} from "../../Container";
import {AfterContentInit} from "angular2/core";
import {HTTP_PROVIDERS} from "angular2/http";
import {ROUTER_DIRECTIVES} from "angular2/router";
import {ListModel} from "./ListModel";
import {CORE_DIRECTIVES} from "angular2/common";
import {BreadcrumbComponent} from "../../breadcrumb/breadcrumb.component";
import {BreadcrumbService} from "../../services/breadcrumb.service";
/**
 * Created by nursultan on 3/2/16.
 */
@Component({
    selector: 'list-list-component',
    templateUrl: 'app/admin/list/list-list.html',
    providers: [ListService, Container, HTTP_PROVIDERS],
    styleUrls: ['app/admin/list/list.css'],
    directives: [ROUTER_DIRECTIVES, CORE_DIRECTIVES, BreadcrumbComponent]
})
export class ListListComponent implements OnInit, AfterContentInit {
    public lists:any;
    public loading:boolean;
    public newList:ListModel;
    public listToBeDeleted:ListModel;
    public addListClicked:boolean;

    constructor(private listService:ListService,
                private _breadcrumbService:BreadcrumbService) {
        this.addListClicked = false;
    }

    ngAfterContentInit():any {
        return undefined;
    }


    ngOnInit():any {
        this.loading = false;
        this.listService.getLists()
            .subscribe(listList => this.lists = listList);
        var newValue = {key: 'Lists', last: 'Countries'};
        this._breadcrumbService.broadcastComponentChange(newValue);
        return undefined;

    }

    addList() {
        this.addListClicked = true;
        this.newList = <ListModel>{ListName: '', BuiltIn: false, Items: []};
    }

    onDelete(list:ListModel) {
        this.listToBeDeleted = list;
        jQuery('#modal-delete').openModal();
    }

    onDeleteYes() {
        jQuery('#modal-delete').closeModal();
        this.listService.delete(this.listToBeDeleted)
            .subscribe(res => {
                this.lists = this.lists.filter(element => element.ID != this.listToBeDeleted.ID);
                Materialize.toast('List has been successfully successfully deleted', 4000);
            }, this.showErrorToast);
    }

    showErrorToast() {
        Materialize.toast('An error occurred, please try again', 4000);
    }
}